#include "compile-errors.h"
#include <stdlib.h>

void swInitCompileErrors (swCompileErrors* errors)
{
	errors->count = 0;
	errors->capacity = 16;
	errors->errors = malloc(sizeof(swCompileError) * errors->capacity);
};

void swQuitCompileErrors (swCompileErrors* errors)
{
	for (size_t i = 0; i < errors->count; i++)
		free(errors->errors[i].message);
	free(errors->errors);
};

void swThrowCompileError (swCompileErrors* errors, char* message, size_t line, size_t col)
{
	if (!errors)
	{
		free(message);
		return;
	};

	if (errors->count >= errors->capacity)
		errors->errors = realloc(errors->errors,sizeof(swCompileError) * (errors->capacity <<= 1));
	swCompileError* error = errors->errors + errors->count++;
	error->message = message;
	error->line = line;
	error->col = col;
};
