#ifndef PARSE_H_INCLUDED
#define PARSE_H_INCLUDED

#include "lex.h"
#include "compile-errors.h"

enum PrimitiveType
{
	Type_UnsignedByte,
	Type_SignedByte,
	Type_UnsignedShort,
	Type_SignedShort,
	Type_UnsignedInt,
	Type_SignedInt,
	Type_UnsignedLong,
	Type_SignedLong,
	Type_Float,
	Type_Double,
	Type_String
};

typedef struct Var Var;

typedef struct
{
	int arg_count;
	Var* arg_vars;
	swToken* return_typename; // NULL means no return type
}
FunctionSignature;

struct Var
{
	swToken* identifier;
	swToken* type_name;
	FunctionSignature* func_signature; // used if type_name is NULL, indicating function type
	int pointer_level;
		// int x; -> ptr_level = 0
		// int* x; -> ptr_level = 1
		// int** x; -> ptr_level = 2
		// function foo (float) -> float; -> ptr_level = 0
		// function* foo (float) -> float; -> ptr_level = 1
		// function** foo (float) -> float; -> ptr_level = 2
};

struct Type
{
	char variant;
	union
	{
		int primitive_type;
		BlockData* class_block;
	};
	swToken* identifier;
};

enum ActionType
{
	Action_Statement,
	Action_If,
	Action_Loop, // unconditional loop: "int x = 5; loop { if (!x--) break; }"
	Action_While,
	Action_OnceThenWhile,
	Action_Break,
	Action_Continue,
	Action_Goto,
	Action_Label,
	Action_CodeBlock,
	Action_ClassBlock,
	Action_FuncDef,
	Action_Return,
};

typedef struct Action Action;

typedef struct BlockData BlockData;
struct BlockData
{
	Action** actions;
	int action_count, action_capacity;

	Var* vars;
	int var_count,var_capacity;

	Type* types;
	int type_count,type_capacity;

	BlockData* parent_block;
};
BlockData* ReadBlock (swTokens* tokens, swCompileErrors* compile_errors, enum TokenType end_token_type);
typedef struct
{
	union
	{
		NodesMem* cond_nodesmem;
		ExprTree* cond_expr;
	};
	Action* within; // allowed to be NULL, i.e. "if (x);"
	Action* else_action; // May be NULL if there was no "else".
}
IfData;
typedef struct
{
	union
	{
		NodesMem* cond_nodesmem;
		ExprTree* cond_expr;
	};
	Action* within; // allowed to be NULL, i.e. "while (x);" or even "once then while (x);"
}
WhileData;

typedef struct
{
	BlockData* function_code;
	swToken* function_name;
}
FuncDefData;

struct Action
{
	int type;
	union
	{
		NodesMem* statement_nodesmem; // Will exist even if it's empty. Gets replaced by statement_expr in later parsing.
		ExprTree* statement_expr; // This one is allowed to be NULL if the expression is not valid!
		NodesMem* return_nodesmem; // Allowed to be NULL for "return;"
		ExprTree* return_expr;
		BlockData* block;
		IfData if_header;
		WhileData while_header;
		Action* simple_loop_within; // for simple "loop { }" construct. i.e. type == Action_Loop
		swToken* label_name; // used by Action_Goto and Action_Label
		FuncDefData funcdef_header;
	};
};

void AbortExprNode (ExprNode* node);
void NodesMemAbort (NodesMem* mem);
void ExprTreeFree (ExprTree* tree);
Type* ResolveTypeRecursive (BlockData* scope, swToken* identifier);
FunctionArgsList* ReadFunctionArgs (swTokens* tokens, swCompileErrors* errors);

NodesMem* swReadExpr (swTokens* tokens, swCompileErrors* errors, enum TokenType end_expected, enum TokenType* alternate_end);

void BuildBlockExpressions (swCompileErrors* errors, BlockData* block);

void PrintExprTree (ExprTree* tree, int tabs);

#endif // PARSE_H_INCLUDED
