#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>



typedef struct swStream swStream;
struct swStream
{
	char* cancel_buf;
	size_t cancel_usage,cancel_capacity;
	void* implementor_data;
	int (*get_char) (void* implementor_data);
	void (*cleanup) (void* implementor_data);
	size_t line; // starts at 1
};
void swStreamInit (swStream* stream, void* implementor_data, int (*get_char) (void* implementor_data), void (*cleanup) (void* implementor_data))
{
	stream->cancel_buf = malloc(stream->cancel_capacity = 1);
	stream->cancel_usage = 0;
	stream->implementor_data = implementor_data;
	stream->get_char = get_char;
	stream->cleanup = cleanup;
	stream->line = 1;
};
void swStreamQuit (swStream* stream)
{
	free(stream->cancel_buf);
	stream->cleanup(stream->implementor_data);
};
void swStreamInitFILE (swStream* stream, FILE* file)
{
	swStreamInit(stream,file,fgetc,fclose);
};
int swStreamNext (swStream* stream)
{
	int got;
	if (stream->cancel_usage) got = stream->cancel_buf[--stream->cancel_usage];
	else got = stream->get_char(stream->implementor_data);
	if (got == '\n') stream->line++;
	return got;
};
void swStreamUnget (swStream* stream, int unget)
{
	if (unget == EOF) return;
	if (stream->cancel_usage >= stream->cancel_capacity)
		stream->cancel_buf = realloc(stream->cancel_buf,stream->cancel_capacity <<= 1);
	stream->cancel_buf[stream->cancel_usage++] = unget;
	if (unget == '\n') stream->line--;
};



enum
{
	TOKEN_EndOfFile,
	TOKEN_Identifier,
	TOKEN_StringLiteral,

	TOKEN_RealLiteral,
	TOKEN_IntegerLiteral,
	TOKEN_Dot,

};
typedef struct swToken swToken;
struct swToken
{
	int type;
	char* lexeme;
	size_t lexeme_len;
	size_t line;
	union
	{
		double real_value;
		long long int integer_value;
	};
};



typedef struct swLexer swLexer;
struct swLexer
{
	swStream* stream;
	swToken* cancel_buf;
	size_t cancel_usage,cancel_capacity;
	char* lexeme_buf;
	size_t lexeme_usage,lexeme_capacity;
};
void swLexerInit (swLexer* lexer, swStream* stream)
{
	lexer->cancel_buf = malloc(sizeof(swToken) * (lexer->cancel_capacity = 1));
	lexer->cancel_usage = 0;
	lexer->lexeme_buf = malloc(lexer->lexeme_capacity = 64);
	lexer->lexeme_usage = 0;
	lexer->stream = stream;
};
void swLexerQuit (swLexer* lexer)
{
	free(lexer->cancel_buf);
	free(lexer->lexeme_buf);
	swStreamQuit(lexer->stream);
};
static void swLexerAddChar (swLexer* lexer, char add)
{
	if (lexer->lexeme_usage >= lexer->lexeme_capacity)
		lexer->lexeme_buf = realloc(lexer->lexeme_buf,lexer->lexeme_capacity <<= 1);
	lexer->lexeme_buf[lexer->lexeme_usage++] = add;
};
static swToken swLexerExport (swLexer* lexer, int type)
{
	swToken token;
	token.type = type;
	token.lexeme = malloc(token.lexeme_len = lexer->lexeme_usage);
	memcpy(token.lexeme,lexer->lexeme_buf,token.lexeme_len);
	lexer->lexeme_usage = 0;
	token.line = lexer->stream->line;
	return token;
};
void swLexerUnget (swLexer* lexer, swToken unget)
{
	if (unget.type == TOKEN_EndOfFile) return;
	if (lexer->cancel_usage >= lexer->cancel_capacity)
		lexer->cancel_buf = realloc(lexer->cancel_buf,sizeof(swToken) * (lexer->cancel_capacity <<= 1));
	lexer->cancel_buf[lexer->cancel_usage++] = unget;
};
static void swLexerSkipSpaces (swLexer* lexer)
{
	while (1)
	{
		int here = swStreamNext(lexer->stream);
		if (!isspace(here))
		{
			swStreamUnget(lexer->stream,here);
			break;
		};
	};
};
int swRigidTokenMatch (swLexer* lexer)
{
	int first = swStreamNext(lexer->stream);
	switch (first)
	{
		case '(': return TOKEN_LParen;
		case ')': return TOKEN_RParen;
		case '[': return TOKEN_LBracket;
		case ']': return TOKEN_RBracket;
		case '{': return TOKEN_LBrace;
		case '}': return TOKEN_RBrace;
		case ':': return TOKEN_Colon;
		case ';': return TOKEN_Semicolon;
		case ',': return TOKEN_Comma;
	};
	int second = swStreamNext(lexer->stream);
	switch (first)
	{
		case '=': 	switch (second)
					{
						case '=': return TOKEN_Equal;
						default: swStreamUnget(lexer->stream,second); return TOKEN_Assign;
					};
		case '!':	switch (second)
					{
						case '=': return TOKEN_Unequal;
						default: swStreamUnget(lexer->stream,second); return TOKEN_Not;
					};
		case '+':	switch (second)
					{
						case '+': return TOKEN_AddOne;
						case '=': return TOKEN_AddBy;
						default: swStreamUnget(lexer->stream,second); return TOKEN_Add;
					};
		case '-':	switch (second)
					{
						case '-': return TOKEN_SubOne;
						case '=': return TOKEN_SubBy;
						default: swStreamUnget(lexer->stream,second); return TOKEN_Sub;
					};
		case '*':	switch (second)
					{
						case '=': return TOKEN_MulBy;
						default: swStreamUnget(lexer->stream,second); return TOKEN_Mul;
					};
		case '/':	switch (second)
					{
						case '=': return TOKEN_DivBy;
						default: swStreamUnget(lexer->stream,second); return TOKEN_Div;
					};
	};
	swStreamUnget(lexer->stream,second);
	return TOKEN_Invalid;
};
swToken swLexerNext (swLexer* lexer)
{
	if (lexer->cancel_usage) return lexer->cancel_buf[lexer->cancel_usage--];

	swLexerSkipSpaces(lexer);

	int first = swStreamNext(lexer->stream);
	if (first == EOF) return swLexerExport(lexer,TOKEN_EndOfFile);
	else if (first == '_' || isalpha(first))
	{
		swLexerAddChar(lexer,first);
		swLexerReadIdentifier(lexer);
		return swLexerExport(lexer,TOKEN_Identifier);
	}
	else if (first == '.' || isdigit(first))
	{
		swLexerAddChar(lexer,first);
		swLexerReadNumber(lexer);
		return swResolveNumber(lexer);



	else if (swIsIdentifierStart(here))
	{
		token.len = swReadIdentifier(src,&pos,&line_number,&line_position);
		swIdentifyKeyword(&token);
	}
	else if (swIsNumericStart(here))
	{
		token.len = swReadNumber(src,&pos,&line_number,&line_position);
		swInterpretNumber(&token);
	}
	else if (swStringLiteralStart(here))
	{
		token.len = swReadStringLiteral(compile_errors,src,&pos,&line_number,&line_position);
		swRefineStringLiteral(compile_errors,&token);
	}
	else
	{
		//token.len = 1;
		switch (here)
		{
			case '(': token.type = OpenParentheses; break;
			case ')': token.type = CloseParentheses; break;
			case '[': token.type = OpenBrackets; break;
			case ']': token.type = CloseBrackets; break;
			case '{': token.type = OpenBraces; break;
			case '}': token.type = CloseBraces; break;
			case ';': token.type = Semicolon; break;
			case ',': token.type = Comma; break;
			default:
			token.opcode = swOpIDHere(token.str,&pos,&line_number,&line_position);
			if (token.opcode)
			{
				token.type = Operator;
				//token.len = 0;
				goto DONT_INCREMENT;
			}
			else
			{
				token.type = IllegalCharacter;
				//token.len = 1;
			};
		};
		pos++;
		line_position++;
		DONT_INCREMENT:;
	};
	return token;
};
	int first = swStreamNext(lexer->stream);
	if (first == EOF) return swLexerExport(lexer,TOKEN_EndOfFile);

void swInitTokens (swTokens* tokens)
{
	tokens->count = 0;
	tokens->capacity = 1024;
	tokens->tokens = malloc(sizeof(swToken) * tokens->capacity);
	tokens->read_pos = 0;
};

void swQuitTokens (swTokens* tokens)
{
	// Every swToken's string field, if applicable, points to a position
	// in the original source code string. Aside from being unnecessary,
	// to pass them to `free` would probably cause big problems.
	free(tokens->tokens);
};

void swAppendToken (swTokens* tokens, swToken* token)
{
	if (tokens->count >= tokens->capacity)
		tokens->tokens = realloc(tokens->tokens,sizeof(swToken) * (tokens->capacity <<= 1));
	tokens->tokens[tokens->count++] = *token;
};

swToken* swNextToken (swTokens* tokens)
{
	int offset = tokens->read_pos++;
	if (offset >= tokens->count) offset = tokens->count - 1; // Point to the trailing EOF.
	return tokens->tokens + offset;
};

void swUngetToken (swTokens* tokens)
{
	tokens->read_pos--;
};

void swReadAllTokens (swTokens* tokens, char* src, swCompileErrors* compile_errors, char emit_whitespace)
{
	size_t pos = 0;
	size_t line_number = 1, line_position = 0;
	char keepgoing = 1;
	while (keepgoing)
	{
		swToken token;
		token.str = src + pos;
		char here = *token.str;
		token.line_number = line_number;
		token.line_position = line_position;
		if (!here)
		{
			token.type = EndOfFile;
			//token.len = 0;
			keepgoing = 0;
		}
		else if (swIsWhitespaceStart(here))
		{
			token.len = swReadWhitespace(src,&pos,&line_number,&line_position);
			if (!emit_whitespace) continue;
			token.type = Whitespace;
		}
		else if (swIsIdentifierStart(here))
		{
			token.len = swReadIdentifier(src,&pos,&line_number,&line_position);
			swIdentifyKeyword(&token);
		}
		else if (swIsNumericStart(here))
		{
			token.len = swReadNumber(src,&pos,&line_number,&line_position);
			swInterpretNumber(&token);
		}
		else if (swStringLiteralStart(here))
		{
			token.len = swReadStringLiteral(compile_errors,src,&pos,&line_number,&line_position);
			swRefineStringLiteral(compile_errors,&token);
		}
		else
		{
			//token.len = 1;
			switch (here)
			{
				case '(': token.type = OpenParentheses; break;
				case ')': token.type = CloseParentheses; break;
				case '[': token.type = OpenBrackets; break;
				case ']': token.type = CloseBrackets; break;
				case '{': token.type = OpenBraces; break;
				case '}': token.type = CloseBraces; break;
				case ';': token.type = Semicolon; break;
				case ',': token.type = Comma; break;
				default:
				token.opcode = swOpIDHere(token.str,&pos,&line_number,&line_position);
				if (token.opcode)
				{
					token.type = Operator;
					//token.len = 0;
					goto DONT_INCREMENT;
				}
				else
				{
					token.type = IllegalCharacter;
					//token.len = 1;
				};
			};
			pos++;
			line_position++;
			DONT_INCREMENT:;
		};
		swAppendToken(tokens,&token);
	};
};



// For Development Purposes
void swPrintTokenStr (swToken* token)
{
	putchar('"');
	for (int i = 0; i < token->len; i++) putchar(token->str[i]);
	putchar('"');
};

// For Development Purposes
void swPrintToken (swToken* token, int tabs)
{
	mktabs(tabs);
	switch(token->type)
	{
		case Whitespace:
		printf("< w h i t e s p a c e >");
		break;

		case Expression:
		printf("<nested expression: (");
		printf("..."); // to do: actually print expression
		printf(")>");
		break;

		case TypeCast:
		printf("<cast to ");
		swPrintTokenStr(token->typecast_type->identifier);
		printf(">");
		break;

		case FunctionCall:
		printf("<function call>\n");
		mktabs(tabs + 1);
		if (token->function_call_args)
		{
			printf("args (%d):\n",token->function_call_args->count);
			for (int i = 0; i < token->function_call_args->count; i++)
				PrintExprTree(token->function_call_args->expressions[i],tabs + 2);
		}
		else printf("no args\n");
		break;

		case IntLiteral:
		printf("(int) %lld",token->intvalue);
		break;

		case FloatLiteral:
		printf("(float) %lf",token->floatvalue);
		break;

		case StringLiteral:
		printf("(string[%d]) ",token->len);
		swPrintTokenStr(token);
		break;

		case Identifier:
		printf("(identifier[%d]) ",token->len);
		swPrintTokenStr(token);
		break;

		case Operator:;
		char* symname = swGetOperatorSymbol(token->opcode);
		printf("(operator) \"%s\"",symname);
		break;

		case OpenParentheses:
		case CloseParentheses:
		case OpenBrackets:
		case CloseBrackets:
		case OpenBraces:
		case CloseBraces:
		case Semicolon:
		case Comma:
		putchar(*token->str);
		break;

		case IllegalCharacter:
		printf("Illegal character: '%c'",*token->str);
		break;

		case InvalidInt2:
		printf("(invalid base-2 int) ");
		swPrintTokenStr(token);
		break;

		case InvalidInt8:
		printf("(invalid base-8 int) ");
		swPrintTokenStr(token);
		break;

		case InvalidInt10:
		printf("(invalid base-10 int) ");
		swPrintTokenStr(token);
		break;

		case InvalidInt16:
		printf("(invalid base-16 int) ");
		swPrintTokenStr(token);
		break;

		case InvalidFloat:
		printf("(invalid float) ");
		swPrintTokenStr(token);
		break;

		case Keyword_If: printf("if"); break;
		case Keyword_Else: printf("else"); break;
		case Keyword_For: printf("for"); break;
		case Keyword_While: printf("while"); break;
		case Keyword_Null: printf("null"); break;

		case EndOfFile: printf("<EOF>"); break;

		default:
		printf("[Invalid token!]");
	};
};

// For Development Purposes
void swPrintTokens (swTokens* tokens)
{
	printf("%lld tokens:\n",tokens->count);
	swToken* token = tokens->tokens;
	for (int i = 0; i < tokens->count; i++, token++)
	{
		swPrintToken(token,1);
		putchar('\n');
	};
};
