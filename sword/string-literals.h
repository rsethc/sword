#ifndef STRING_LITERALS_H_INCLUDED
#define STRING_LITERALS_H_INCLUDED

#include "lex.h"
#include "compile-errors.h"

char swStringLiteralStart (char ch);
size_t swReadStringLiteral (swCompileErrors* errors, char* src, size_t* pos, size_t* line_number, size_t* line_position);
void swRefineStringLiteral (swCompileErrors* errors, swToken* token);

#endif // STRING_LITERALS_H_INCLUDED
