#include "operators.h"
#include <string.h>
#include "simplestorage.h"

typedef struct
{
	char id;
	char str [4];
}
OpSymbol;
OpSymbol OpSymbols [] =
{
	// =
	{Op_Equal,"=="},
	{Op_Assign,"="},

	// !
	{Op_Unequal,"!="},
	{Op_BoolNot,"!"},

	// <
	{Op_BitShiftLeftBy,"<<="},
	{Op_BitShiftLeft,"<<"},
	{Op_LessThanEqual,"<="},
	{Op_LessThan,"<"},

	// >
	{Op_BitShiftRightBy,">>="},
	{Op_BitShiftRight,">>"},
	{Op_GreaterThanEqual,">="},
	{Op_GreaterThan,">"},

	// +
	{Op_AddBy,"+="},
	{Op_AddOne,"++"},
	{Op_Add,"+"},

	// -
	{Op_RightArrow,"->"},
	{Op_SubtractBy,"-="},
	{Op_SubtractOne,"--"},
	{Op_Subtract,"-"},

	// *
	{Op_MulBy,"*="},
	{Op_Mul,"*"},

	// /
	{Op_DivBy,"/="},
	{Op_Div,"/"},

	// %
	{Op_ModulusBy,"%="},
	{Op_Modulus,"%"},

	// |
	{Op_BoolOrBy,"||="},
	{Op_BoolOr,"||"},
	{Op_BitOrBy,"|="},
	{Op_BitOr,"|"},

	// &
	{Op_BoolAndBy,"&&="},
	{Op_BoolAnd,"&&"},
	{Op_BitAndBy,"&="},
	{Op_BitAnd,"&"},

	// ^
	{Op_BoolXorBy,"^^="},
	{Op_BoolXor,"^^"},
	{Op_BitXorBy,"^="},
	{Op_BitXor,"^"},

	// ~
	{Op_BitNot,"~"},

	// $
	{Op_Stringify,"$"},

	// ?
	{Op_ChooseFirst,"?"},

	// :
	{Op_ChooseSecond,":"},

	// #
	{Op_Length,"#"},

	// This won't be discovered by the operator reading,
	// but have it here for token printing.
	{Op_Dot,"."},
};
int nOpSymbols = sizeof(OpSymbols) / sizeof(OpSymbol);

char swOpIDHere (char* src, size_t* pos, size_t* line_number, size_t* line_position)
{
	for (int i = 0; i < nOpSymbols; i++)
	{
		OpSymbol sym = OpSymbols[i];
		if (StartsWith(src,sym.str))
		{
			size_t symlen = strlen(sym.str);
			*pos += symlen;
			*line_position += symlen;
			return sym.id;
		};
	};
	return 0;
};

char IsUnaryPrefix (char opcode)
{
	switch (opcode)
	{
		case Op_AddOne:
		case Op_BitNot:
		case Op_BoolNot:
		case Op_Length:
		case Op_Stringify:
		case Op_Subtract:
		case Op_SubtractOne:
		return 1;

		default:
		return 0;
	};
};

char IsUnaryPostfix (char opcode)
{
	switch (opcode)
	{
		case Op_AddOne:
		case Op_SubtractOne:
		return 1;

		default:
		return 0;
	};
};

char IsBinaryOperator (char opcode)
{
	switch (opcode)
	{
		case Op_Add:
		case Op_AddBy:
		case Op_Assign:
		case Op_BitAnd:
		case Op_BitAndBy:
		case Op_BitOr:
		case Op_BitOrBy:
		case Op_BitShiftLeft:
		case Op_BitShiftLeftBy:
		case Op_BitShiftRight:
		case Op_BitShiftRightBy:
		case Op_BitXor:
		case Op_BitXorBy:
		case Op_BoolAnd:
		case Op_BoolAndBy:
		case Op_BoolOr:
		case Op_BoolOrBy:
		case Op_BoolXor:
		case Op_BoolXorBy:
		case Op_Div:
		case Op_DivBy:
		case Op_Dot:
		case Op_Equal:
		case Op_GreaterThan:
		case Op_GreaterThanEqual:
		case Op_LessThan:
		case Op_LessThanEqual:
		case Op_Modulus:
		case Op_ModulusBy:
		case Op_Mul:
		case Op_MulBy:
		case Op_Subtract:
		case Op_SubtractBy:
		case Op_Unequal:
		return 1;

		default:
		return 0;
	};
};

// For Development Purposes
char* swGetOperatorSymbol (char opcode)
{
	for (int i = 0; i < nOpSymbols; i++)
		if (OpSymbols[i].id == opcode)
			return OpSymbols[i].str;
	return "[Invalid opcode!]";
};
