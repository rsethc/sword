#include "simplestorage.h"



#include <stdlib.h>
#include <string.h>

void SimpleStorageInit (SimpleStorage* storage, size_t itemsize, size_t initial_items)
{
	storage->itemsize = itemsize;
	if (!initial_items) initial_items = 1;
	storage->capacity = initial_items;
	storage->buf = malloc(itemsize * initial_items);
	storage->count = 0;
};
void SimpleStorageAppend (SimpleStorage* storage, void* copy_in)
{
	if (storage->count >= storage->capacity)
		storage->buf = realloc(storage->buf,storage->itemsize * (storage->capacity <<= 1));
	memcpy(SimpleStoragePtr(storage,storage->count++),copy_in,storage->itemsize);
};
size_t SimpleStorageNext (SimpleStorage* storage)
{
	if (storage->count >= storage->capacity)
		storage->buf = realloc(storage->buf,storage->itemsize * (storage->capacity <<= 1));
	return storage->count++;
};
void SimpleStorageSet (SimpleStorage* storage, size_t index, void* copy_in)
{
	memcpy(SimpleStoragePtr(storage,index),copy_in,storage->itemsize);
};
void SimpleStorageGet (SimpleStorage* storage, size_t index, void* copy_out)
{
	memcpy(copy_out,SimpleStoragePtr(storage,index),storage->itemsize);
};




char StartsWith (char* fullstr, char* startstr)
{
	while (1)
	{
		char fullchar = *fullstr++;
		char startchar = *startstr++;
		if (!startchar) return 1;
		if (fullchar != startchar) return 0;
	};
};
