#include "numbers.h"
#include "operators.h"
#include <ctype.h>

char swIsNumericStart (char ch)
{
	return isdigit(ch) || ch == '.';
};

char swIsHexChar (char ch)
{
	return (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F');
};

char swIsNumericContinuation (char ch, char* isint, char* ishex)
{
	*isint = *isint && ch != '.';
	*ishex = *ishex || (*isint && ch == 'x');
	return
		isdigit(ch) ||
		ch == '.' ||
		(*isint && (ch == 'x' || ch == 'b')) ||
		(*ishex && swIsHexChar(ch));
};

size_t swReadNumber (char* src, size_t* pos, size_t* line_number, size_t* line_position)
{
	size_t pre = *pos;
	char isint = 1;
	char ishex = 0;
	while (swIsNumericContinuation(src[*pos],&isint,&ishex))
	{
		(*pos)++;
		(*line_position)++;
	};
	size_t post = *pos;
	return post - pre;
};

void swInterpretIntBase10 (swToken* token, char* str, int len)
{
	long long int value = 0;
	for (int i = 0; i < len; i++)
	{
		value *= 10;
		char cc = str[i];
		if (cc >= '0' && cc <= '9') value += cc - '0';
		else
		{
			token->type = InvalidInt10;
			return;
		};
	};
	token->intvalue = value;
	token->len = 0;
};

void swInterpretIntBase16 (swToken* token, char* str, int len)
{
	long long int value = 0;
	for (int i = 0; i < len; i++)
	{
		value <<= 4;
		char cc = str[i];
		if (cc >= '0' && cc <= '9') value |= cc - '0';
		else if (cc >= 'a' && cc <= 'f') value |= cc - 'a' + 10;
		else if (cc >= 'A' && cc <= 'F') value |= cc - 'A' + 10;
		else
		{
			token->type = InvalidInt16;
			return;
		};
	};
	token->intvalue = value;
	token->len = 0;
};

void swInterpretIntBase8 (swToken* token, char* str, int len)
{
	long long int value = 0;
	for (int i = 0; i < len; i++)
	{
		value <<= 3;
		char cc = str[i];
		if (cc >= '0' && cc <= '7') value |= cc - '0';
		else
		{
			token->type = InvalidInt8;
			return;
		};
	};
	token->intvalue = value;
	token->len = 0;
};

void swInterpretIntBase2 (swToken* token, char* str, int len)
{
	long long int value = 0;
	for (int i = 0; i < len; i++)
	{
		value <<= 1;
		char cc = str[i];
		if (cc == '1') value |= 1;
		else if (cc != '0')
		{
			token->type = InvalidInt2;
			return;
		};
	};
	token->intvalue = value;
	token->len = 0;
};

void swInterpretInt (swToken* token)
{
	token->type = IntLiteral;
	char* str = token->str;
	int len = token->len;
	if (str[0] == '0') // Pass1 should not generate a numeric literal token with length less than 1.
	{
		if (len >= 2)
		{
			if (str[1] == 'x')
			{
				swInterpretIntBase16(token,str + 2,len - 2);
				return;
			}
			else if (str[1] == 'b')
			{
				swInterpretIntBase2(token,str + 2,len - 2);
				return;
			};
		};
		swInterpretIntBase8(token,str + 1,len - 1);
	}
	else swInterpretIntBase10(token,str,len);
};

void swInterpretFloat (swToken* token)
{
	int pos = 0;
	double primary = 0;
	while (pos < token->len)
	{
		char cc = token->str[pos++];
		if (cc == '.') break;
		else if (cc >= '0' && cc <= '9')
		{
			primary *= 10;
			primary += cc - '0';
		}
		else
		{
			token->type = InvalidFloat;
			return;
		};
	};
	double secondary_power = 0.1;
	while (pos < token->len)
	{
		char cc = token->str[pos++];
		if (cc >= '0' && cc <= '9')
		{
			primary += (cc - '0') * secondary_power;
			secondary_power *= 0.1;
		}
		else
		{
			token->type = InvalidFloat;
			return;
		};
	};
	token->type = FloatLiteral;
	token->floatvalue = primary;
	token->len = 0;
};

void swInterpretNumber (swToken* token)
{
	// Determine if it's floating-point by finding the '.' character.
	for (int i = 0; i < token->len; i++)
	{
		char cc = token->str[i];
		if (cc == '.')
		{
			if (token->len == 1)
			{
				token->type = Operator;
				token->opcode = Op_Dot;
			}
			else swInterpretFloat(token);
			return;
		};
	};
	swInterpretInt(token);
};



