#ifndef WHITESPACE_H_INCLUDED
#define WHITESPACE_H_INCLUDED

#include <stdlib.h>

int swIsWhitespaceStart (char ch);
size_t swReadWhitespace (char* src, size_t* pos, size_t* line_number, size_t* line_position);

#endif // WHITESPACE_H_INCLUDED
