#define _GNU_SOURCE
#include <stdio.h>
#include "string-literals.h"

/*
	"string"
		A string value.
		"\t" -> hard tab character
		"\n" -> line feed character
		"\r" -> carriage return character
		"\\" -> backslash character
		"\"" -> " character
		Not yet supported...
			"\0;" -> null character
			"\8F;" -> 0x8F value as character

	'A'
		An integer value, the raw character value of A.
		'\'' -> value of ' character
		'abc' -> Value is ('a' << 16 | 'b' << 8 | 'c' << 0).

	`copy Box2D\compiled\libbox2d.a "package/lib/libbox2d.a"`
		A string which is interpreted with no escape sequences.
		Useful for shell commands on Windows where paths often must be backslashes,
		or shell commands on any platform where there are lots of " characters,
		since in either case escaping every occurrence with \ is tedious and annoying.
*/

char swStringLiteralStart (char ch)
{
	return ch == '\'' || ch == '"' || ch == '`';
};

size_t swReadStringLiteral (swCompileErrors* errors, char* src, size_t* pos, size_t* line_number, size_t* line_position)
{
	char delim = src[(*pos)++];
	(*line_position)++;
	char can_escape = delim != '`';

	size_t pre = *pos;
	size_t post;
	char escape = 0;
	while (1)
	{
		char here = src[*pos];
		if (!here)
		{
			char* err_msg;
			asprintf(&err_msg,"Expected literal delimiter %c, got <EOF> instead.",delim);
			swThrowCompileError(errors,err_msg,*line_number,*line_position);

			post = *pos;
			break;
		};

		if (escape) escape = 0;
		else if (here == delim)
		{
			post = (*pos)++;
			(*line_position)++;
			break;
		}
		else if (here == '\\' && can_escape) escape = 1;

		(*pos)++;
		if (here == '\n')
		{
			(*line_number)++;
			*line_position = 0;
		}
		else (*line_position)++;
	};
	return post - pre;
};

void swProcessEscapes (swCompileErrors* errors, swToken* token, char delim)
{
	size_t outlen = 0;
	char escape = 0;
	size_t line = token->line_number;
	size_t col = token->line_position;
	for (int i = 0; i < token->len; i++)
	{
		char here = token->str[i];
		if (here == '\n')
		{
			line++;
			col = 0;
		}
		else col++;

		if (here == '\\' && !escape)
		{
			escape = 1;
			continue;
		};

		if (escape)
		{
			escape = 0;
			switch (here)
			{
				case 't': here = '\t'; break;
				case 'n': here = '\n'; break;
				case 'r': here = '\r'; break;
				case '\\': here = '\\'; break;
				default:
				if (here == delim) break;
				// Otherwise:
				{
					char* err_msg;
					asprintf(&err_msg,"Invalid escape sequence in literal: \\%c",here);
					swThrowCompileError(errors,err_msg,line,col);
				};
			};
		};
		token->str[outlen++] = here;
	};
	token->len = outlen;
};

void swCalculateCharacterLiteral (swToken* token)
{
	long long int intvalue = 0;
	for (int i = 0; i < token->len; i++)
	{
		intvalue <<= 8;
		intvalue |= token->str[i];
	};
	token->type = IntLiteral;
	token->intvalue = intvalue;
};

void swRefineStringLiteral (swCompileErrors* errors, swToken* token)
{
	token->type = StringLiteral;
	char delim = *token->str++;
	if (delim == '`') return;
	swProcessEscapes(errors,token,delim);
	if (delim == '\'') swCalculateCharacterLiteral(token);
};
