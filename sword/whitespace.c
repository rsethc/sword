#include "whitespace.h"
#include <ctype.h>

int swIsWhitespaceStart (char ch)
{
	return isspace(ch);
};

size_t swReadWhitespace (char* src, size_t* pos, size_t* line_number, size_t* line_position)
{
	size_t start = *pos;
	char cc;
	while (isspace(cc = src[*pos]))
	{
		if (cc == '\n')
		{
			(*line_number)++;
			*line_position = 0;
		}
		else (*line_position)++;
		(*pos)++;
	};
	size_t end = *pos;
	return end - start;
};
