#ifndef NUMBERS_H_INCLUDED
#define NUMBERS_H_INCLUDED

#include "lex.h"

char swIsNumericStart (char ch);
size_t swReadNumber (char* src, size_t* pos, size_t* line_number, size_t* line_position);
void swInterpretNumber (swToken* token);

#endif // NUMBERS_H_INCLUDED
