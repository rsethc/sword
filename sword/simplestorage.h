#ifndef SIMPLESTORAGE_H_INCLUDED
#define SIMPLESTORAGE_H_INCLUDED



#include <stdlib.h>

typedef struct
{
	void* buf;
	size_t count;
	size_t capacity;
	size_t itemsize; // in bytes
}
SimpleStorage;
void SimpleStorageInit (SimpleStorage* storage, size_t itemsize, size_t initial_items);
#define SimpleStorageQuit(storage) (free((storage)->buf))
#define SimpleStorageReset(storage) ((storage)->count = 0)
#define SimpleStoragePtr(storage,index) ((storage)->buf + (index) * (storage)->itemsize) // Pointer can become invalid due to reallocation.
void SimpleStorageAppend (SimpleStorage* storage, void* copy_in); // Ensures a next space exists, copies item into next space, increments item count.
size_t SimpleStorageNext (SimpleStorage* storage); // Ensures a next space exists, increments item count, returns index of next space.
void SimpleStorageSet (SimpleStorage* storage, size_t index, void* copy_in); // Copies item into space specified by index.
void SimpleStorageGet (SimpleStorage* storage, size_t index, void* copy_out); // Copies item from space specified by index.



char StartsWith (char* fullstr, char* startstr);



#endif // SIMPLESTORAGE_H_INCLUDED
