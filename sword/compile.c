#include "compile.h"

void PrintBlock (BlockData* block, int tab_level);

void PrintAction (Action* action, int tab_level)
{
	switch (action->type)
	{
		case Action_ClassBlock:
		mktabs(tab_level);
		printf("class def\n");
		break;

		case Action_CodeBlock:
		PrintBlock(action->block,tab_level);
		break;

		case Action_If:
		mktabs(tab_level);
		printf("if statement\n");
		mktabs(tab_level);
		printf("Condition:\n");
		PrintExprTree(action->if_header.cond_expr,tab_level + 1);
		mktabs(tab_level);
		if (action->if_header.within)
		{
			printf("Within:\n");
			PrintAction(action->if_header.within,tab_level + 1);
		}
		else printf("Nothing Within\n");
		if (action->if_header.else_action)
		{
			mktabs(tab_level);
			printf("Else:\n");
			PrintAction(action->if_header.else_action,tab_level + 1);
		};
		break;

		case Action_Loop:
		mktabs(tab_level);
		printf("unconditional loop\n");
		mktabs(tab_level);
		if (action->simple_loop_within)
		{
			printf("Within:\n");
			PrintAction(action->simple_loop_within,tab_level + 1);
		}
		else printf("Nothing Within\n");
		break;

		case Action_While:
		mktabs(tab_level);
		printf("while loop\n");
		mktabs(tab_level);
		printf("Condition:\n");
		PrintExprTree(action->while_header.cond_expr,tab_level + 1);
		mktabs(tab_level);
		if (action->while_header.within)
		{
			printf("Within:\n");
			PrintAction(action->while_header.within,tab_level + 1);
		}
		else printf("Nothing Within\n");
		break;

		case Action_OnceThenWhile:
		mktabs(tab_level);
		printf("once-then-while loop\n");
		mktabs(tab_level);
		printf("Condition:\n");
		PrintExprTree(action->while_header.cond_expr,tab_level + 1);
		mktabs(tab_level);
		if (action->while_header.within)
		{
			printf("Within:\n");
			PrintAction(action->while_header.within,tab_level + 1);
		}
		else printf("Nothing Within\n");
		break;

		case Action_Return:
		mktabs(tab_level);
		printf("return statement\n");
		if (action->return_expr) PrintExprTree(action->return_expr,tab_level + 1);
		break;

		case Action_Statement:
		mktabs(tab_level);
		printf("statement:\n");
		PrintExprTree(action->statement_expr,tab_level + 1);
		break;

		case Action_FuncDef:
		mktabs(tab_level);
		printf("function definition:\n");
		swPrintToken(action->funcdef_header.function_name,tab_level + 1);
		PrintBlock(action->funcdef_header.function_code,tab_level + 1);
		break;

		default:
		mktabs(tab_level);
		printf("[!] UNRECOGNIZED ACTION TYPE\n");
	};
};

void PrintFuncSignature (FunctionSignature* signature)
{
	printf("function (");
	for (int i = 0; i < signature->arg_count; i++)
	{
		if (i) printf(", ");
		Var* var = signature->arg_vars + i;
		swPrintToken(var->identifier,0);
		printf(": ");
		swPrintToken(var->type_name,0);
	};
	printf(")");
	if (signature->return_typename)
	{
		printf(" -> ");
		swPrintToken(signature->return_typename,0);
	};
};

void PrintBlock (BlockData* block, int tab_level)
{
	mktabs(tab_level);
	printf("block...\n");

	mktabs(tab_level);
	printf("%d class defs:\n",block->type_count);
	for (int i = 0; i < block->type_count; i++)
	{
		Type* type = block->types + i;
		mktabs(tab_level + 1);
		printf("class named ");
		swPrintToken(type->identifier,0);
		printf(":\n");
		PrintBlock(type->class_block,tab_level + 2);
	};

	mktabs(tab_level);
	printf("%d vars:\n",block->var_count);
	for (int i = 0; i < block->var_count; i++)
	{
		Var* var = block->vars + i;
		swPrintToken(var->identifier,tab_level + 1);
		printf(": ");
		if (var->type_name) swPrintToken(var->type_name,0);
		else PrintFuncSignature(var->func_signature);
		printf("\n");
	};

	mktabs(tab_level);
	printf("%d actions:\n",block->action_count);
	for (int i = 0; i < block->action_count; i++)
	{
		Action* action = block->actions[i];
		PrintAction(action,tab_level + 1);
	};
};

BlockData* swCompileTokens (swTokens* tokens, swCompileErrors* compile_errors)
{
	BlockData* script_main = ReadBlock(tokens,compile_errors,EndOfFile);
	BuildBlockExpressions(compile_errors,script_main);
	PrintBlock(script_main,0);
	return script_main;
};
