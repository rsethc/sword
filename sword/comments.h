#ifndef COMMENTS_H_INCLUDED
#define COMMENTS_H_INCLUDED

#include "compile-errors.h"

/// These are intended for visualizing comments in a text editor. Not needed for compilation.
typedef struct
{
	size_t start_line,start_col,stop_line,stop_col;
}
CommentSpan;
typedef struct
{
	CommentSpan* comments;
	int count,capacity;
}
CommentMap;

void QuitCommentMap (CommentMap* comment_map);

void swRemoveComments (char* src, swCompileErrors* errors, CommentMap* comment_map);
/// Can provide NULL to comment_map to forego generating it. Otherwise, you should call QuitCommentMap when you're done with it.

#endif // COMMENTS_H_INCLUDED
