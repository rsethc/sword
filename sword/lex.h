#ifndef LEX_H_INCLUDED
#define LEX_H_INCLUDED

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include "compile-errors.h"

enum TokenType
{
	Token_None, // just to occupy zero

	Whitespace, // Is produced by lexer if emit_whitespace is nonzero. Is not expected by the parser.

	OpenParentheses,
	CloseParentheses,
	OpenBrackets,
	CloseBrackets,
	OpenBraces,
	CloseBraces,
	Semicolon,
	Comma,

	Identifier,
	Operator,
	StringLiteral,
	IntLiteral,
	FloatLiteral,

	InvalidInt2,
	InvalidInt8,
	InvalidInt10,
	InvalidInt16,
	InvalidFloat,
	IllegalCharacter,

	Keyword_If,
	Keyword_Else,
	Keyword_Loop,
	Keyword_While,
	Keyword_Once,
	Keyword_Then,
	Keyword_For,
	Keyword_Break,
	Keyword_Continue,
	Keyword_Goto,
	Keyword_Function,
	Keyword_Return,
	Keyword_Class,
	Keyword_Null,

	EndOfFile,



	// Produced in the initial parsing stage (rather than lexing):

	FunctionCall,
	ParenthesizedSection,

	// Produced by late parsing stage (after all tokens are parsed):

	TypeCast,
	Expression,
};

typedef struct NodesMem NodesMem;

typedef struct ExprTree ExprTree;
typedef struct
{
	union
	{
		NodesMem** nodes_mems;
		ExprTree** expressions;
	};
	int count;
}
FunctionArgsList;

typedef struct swToken swToken;

typedef struct BlockData BlockData;

enum
{
	Primitive,
	//Function,
	Class,
};

typedef struct Type Type;

struct swToken
{
	char type;
	int len;
	union
	{
		char* str;
		double floatvalue;
		long long int intvalue;
		char opcode;
		char keyword;
		Type* typecast_type;

		// Used for ParenthesizedSection
		NodesMem* nodes_mem; // This is independently allocated so it should be freed.
		// Will exist even if it's empty.

		// Used for Expression
		ExprTree* expr_tree; // This is independently allocated so it should be freed.
		// may be NULL, if the expression was not valid

		FunctionArgsList* function_call_args; // Also independently allocated and should be freed.
		// Note: in the case of "foo()" where the token type is FunctionCall, the function_call_args pointer will be NULL.
		// this avoids wasting allocation overhead (time and space) on an empty list.
	};
	int line_number,line_position;
};

typedef struct ExprNode ExprNode;
struct ExprNode
{
	swToken* token;

	// left or right being NULL distinguishes the operator variant
	// left=NULL,right=0x... means ++ is a pre-increment, whereas left=0x...,right=NULL means ++ is a post-increment
	// left=NULL,right=0x... means - is a unary minus, whereas left=0x...,right=0x... means - is a binary minus
	// left *AND* right both being null means this hasn't become a part of the expression tree yet.
	ExprNode* left;
	ExprNode* right;

	// nodes are stored in a linked list as they're being built into a tree
	ExprNode* prev;
	ExprNode* next;
};
struct ExprTree
{
	ExprNode* root;
	ExprNode* nodes_mem; // all nodes in this tree are allocated as one buffer. this is the buffer address.
};


typedef struct
{
	swToken* tokens;
	size_t count,capacity;
	int read_pos;
}
swTokens;

void swInitTokens (swTokens* tokens);
void swQuitTokens (swTokens* tokens);
void swAppendToken (swTokens* tokens, swToken* token);
swToken* swNextToken (swTokens* tokens);
void swUngetToken (swTokens* tokens);

#define mktabs(tabs) { for (int i = 0; i < (tabs); i++) putchar('\t'); }
void swReadAllTokens (swTokens* tokens, char* src, swCompileErrors* compile_errors, char emit_whitespace);
void swPrintTokenStr (swToken* token);
void swPrintToken (swToken* token, int tabs);
void swPrintTokens (swTokens* tokens);

#endif // LEX_H_INCLUDED
