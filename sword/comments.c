#define _GNU_SOURCE
#include <stdio.h>
#include "comments.h"


void InitCommentMap (CommentMap* comment_map)
{
	comment_map->capacity = 16;
	comment_map->comments = malloc(sizeof(CommentSpan) * comment_map->capacity);
	comment_map->count = 0;
};
CommentSpan* CommentMapAppend (CommentMap* comment_map)
{
	if (comment_map->count >= comment_map->capacity)
		comment_map->comments = realloc(comment_map->comments,sizeof(CommentSpan) * (comment_map->capacity <<= 1));
	return comment_map->comments + comment_map->count++;
};
void QuitCommentMap (CommentMap* comment_map)
{
	free(comment_map->comments);
};

// Returns 0 on success, 1 on failure.
void swRemoveComments (char* src, swCompileErrors* errors, CommentMap* map)
{
	/// To do: ignore /*, */, // inside of literals ("",'',``).

	// If map is not NULL, initialize it and write locations of comments to it.
	if (map) InitCommentMap(map);

	int line = 1, column = 0;
	char here;
	char prev = 0;
	while (here = *src++)
	{
		if (here == '\n')
		{
			line++;
			column = 0;
		}
		else if (here == '"' || here == '\'' || here == '`')
		{
			char delim = here;
			char escape = 0;
			while (here = *src)
			{
				if (here) src++;

				if (escape) escape = 0;
				else if (here == delim) break;
				else if (here == '\\' && delim != '`') escape = 1;
			};
		}
		else
		{
			column++;
			if (prev == '/' && here == '/')
			{
				CommentSpan* span;
				if (map)
				{
					span = CommentMapAppend(map);
					span->start_line = line;
					span->start_col = column - 2;
				};
				src -= 2;
				while (1)
				{
					here = *src;
					if (!here) break;
					if (here == '\n')
					{
						line++;
						column = 0;
						src++;
						break;
					}
					else column++;
					*src++ = ' ';
				};
				if (map)
				{
					span->stop_line = line;
					span->stop_col = column;
				};
			}
			else if (prev == '/' && here == '*')
			{
				int started_at_line = line;
				int started_at_column = column - 2;
				CommentSpan* span;
				if (map)
				{
					span = CommentMapAppend(map);
					span->start_line = started_at_line;
					span->start_col = started_at_column;
				};
				src[-2] = ' ';
				src[-1] = ' ';
				prev = 0;
				while (1)
				{
					here = *src;
					if (!here)
					{
						char* err_msg;
						asprintf(&err_msg,"Block comment opened with /* but never closed with */.");
						swThrowCompileError(errors,err_msg,started_at_line,started_at_column);

						if (map)
						{
							span->stop_line = line;
							span->stop_col = column;
						};
						return;
					};
					if (here == '\n')
					{
						line++;
						column = 0;
					}
					else
					{
						column++;
						*src = ' ';
					};
					src++;
					if (prev == '*' && here == '/') break;
					prev = here;
				};
				here = 0;
				if (map)
				{
					span->stop_line = line;
					span->stop_col = column;
				};
			};
		};
		prev = here;
	};
};
