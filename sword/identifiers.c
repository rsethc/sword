#include "identifiers.h"
#include <ctype.h>

int swIsIdentifierStart (char ch)
{
	return isalpha(ch);
};

size_t swReadIdentifier (char* src, size_t* pos, size_t* line_number, size_t* line_position)
{
	size_t pre = *pos;
	while (isalnum(src[*pos]))
	{
		(*pos)++;
		(*line_position)++;
	};
	size_t post = *pos;
	return post - pre;
};

typedef struct
{
	char* str;
	char code;
}
KeywordBind;
KeywordBind KeywordBinds [] =
{
	{"if",Keyword_If},
	{"else",Keyword_Else},
	{"loop",Keyword_Loop},
	{"while",Keyword_While},
	{"once",Keyword_Once},
	{"then",Keyword_Then},
	{"for",Keyword_For},
	{"break",Keyword_Break},
	{"continue",Keyword_Continue},
	{"goto",Keyword_Goto},
	{"function",Keyword_Function},
	{"return",Keyword_Return},
	{"class",Keyword_Class},
	{"null",Keyword_Null},
};
int nKeywordBinds = sizeof(KeywordBinds) / sizeof(KeywordBind);

char swEqualToNullTerminated (char* bufstr, int bufstrlen, char* nulstr)
{
	for (int i = 0; i < bufstrlen; i++)
	{
		char bufcc = bufstr[i];
		char nulcc = *nulstr++;
		if (bufcc != nulcc || !nulcc) return 0;
	};
	return !*nulstr;
};

void swIdentifyKeyword (swToken* token)
{
	for (int i = 0; i < nKeywordBinds; i++)
	{
		KeywordBind binding = KeywordBinds[i];
		if (swEqualToNullTerminated(token->str,token->len,binding.str))
		{
			token->type = binding.code;
			//token->len = 0;
			return;
		};
	};
	token->type = Identifier;
};


