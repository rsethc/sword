#ifndef COMPILE_ERRORS_H_INCLUDED
#define COMPILE_ERRORS_H_INCLUDED

#include <stdlib.h>

typedef struct
{
	char* message;
	size_t line,col;
}
swCompileError;

typedef struct
{
	swCompileError* errors;
	size_t count,capacity;
}
swCompileErrors;

void swInitCompileErrors (swCompileErrors* errors);
void swQuitCompileErrors (swCompileErrors* errors);
void swThrowCompileError (swCompileErrors* errors, char* message, size_t line, size_t col);

#endif // COMPILE_ERRORS_H_INCLUDED
