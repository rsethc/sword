#ifndef COMPILE_H_INCLUDED
#define COMPILE_H_INCLUDED

#include "parse.h"

BlockData* swCompileTokens (swTokens* tokens, swCompileErrors* compile_errors);

#endif // COMPILE_H_INCLUDED
