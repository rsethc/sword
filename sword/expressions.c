#define _GNU_SOURCE
#include <stdio.h>

#include "lex.h"
#include "operators.h"
#include "parse.h"

struct NodesMem // temporarily used inside swReadExpr. the capacity/len info & double linked list are no longer needed once expression tree is built.
{
	ExprNode* nodes;
	size_t capacity,len;

	// double linked list
	ExprNode* first;
	ExprNode* last;

	size_t start_line,start_col; // for compile errors
};
void NodesMemInit (NodesMem* mem)
{
	mem->capacity = 1;
	mem->nodes = malloc(sizeof(ExprNode) * mem->capacity);
	mem->len = 0;
};
void NodesMemAppend (NodesMem* mem, swToken* token)
{
	if (mem->len >= mem->capacity) mem->nodes = realloc(mem->nodes,sizeof(ExprNode) * (mem->capacity <<= 1));
	ExprNode* node = mem->nodes + mem->len++;
	node->left = NULL;
	node->right = NULL;
	node->token = token;
};
void NodesMemSetup (NodesMem* mem)
{
	// sets up pointers for double linked list once everything has been added

	if (!mem->len)
	{
		mem->first = NULL;
		mem->last = NULL;
		return;
	};

	mem->first = mem->nodes;
	size_t last_id = mem->len - 1;
	mem->last = mem->nodes + last_id;

	for (size_t i = 0; i < mem->len; i++)
	{
		ExprNode* node = mem->nodes + i;
		node->prev = i ? node - 1 : NULL;
		node->next = (i < last_id) ? node + 1 : NULL;
	};
};
void NodesMemRemove (NodesMem* mem, ExprNode* node)
{
	ExprNode** from_before = node->prev ? &node->prev->next : &mem->first;
	ExprNode** from_after = node->next ? &node->next->prev : &mem->last;
	*from_before = node->next;
	*from_after = node->prev;
	mem->len--;
};
void NodesMemAbort (NodesMem* mem)
{
	// this is called if the expression can't be parsed. free everything in the nodes.
	for (ExprNode* node = mem->first; node; node = node->next) AbortExprNode(node);
	free(mem->nodes);
};
char IsOperand (ExprNode* node)
{
	if (node)
	{
		switch (node->token->type)
		{
			case Operator:
			case FunctionCall:
			case TypeCast:
			return node->left || node->right;
			// it's only an operand if it's already part of the tree.
			// otherwise it's still a lone operator, not an operand.
		};
		return 1; // it's some literal or varname, so it's an operand.
	};
	return 0; // if it doesn't exist, it's not an operand.
};
char IsOperator (ExprNode* node)
{
	/*if (node)*/ return node->token->type == Operator && !node->left && !node->right;
	//return 0;
};
void PrintExprNode (ExprNode* node, int tabs)
{
	swPrintToken(node->token,tabs);
	putchar('\n');
	if (node->left)
	{
		mktabs(tabs);
		printf("Left:\n");
		PrintExprNode(node->left,tabs+1);
	};
	if (node->right)
	{
		mktabs(tabs);
		printf("Right:\n");
		PrintExprNode(node->right,tabs+1);
	};
};
void PrintNodesMem (NodesMem* list, int tabs)
{
	for (ExprNode* node = list->first; node; node = node->next)
		PrintExprNode(node,tabs);
};
void PrintExprTree (ExprTree* tree, int tabs)
{
	if (tree)
	{
		NodesMem tmp;
		tmp.first = tree->root;
		PrintNodesMem(&tmp,tabs);
	}
	else
	{
		mktabs(tabs);
		printf("<invalid expression>\n");
	};
};
void NodesMemAssemble (NodesMem* list)
{
	// This function only cares about assembling operands and operands.
	// It does not care about operand types, etc.
	// For example "++a++" or "a.7" or "#5" will be allowed.
	// Catching that stuff happens later in compilation.

	// idc that this is dirty looking code, i'll fix it later maybe
	goto BEGIN;
	MALFORMED:
	list->len = 0;
	return;
	BEGIN:

	/* go over the nodes_mem list for each operator with less and less precedence, grouping nodes into each other */

	/// attribute access
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_Dot: case Op_RightArrow: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a.b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// function calls
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (node->token->type == FunctionCall)
		{
			if (!IsOperand(node->prev)) goto MALFORMED; // f()
			node->left = node->prev;
			NodesMemRemove(list,node->left);
		};
	};

	/// modifying unary: ++, --
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_AddOne: case Op_SubtractOne: break;
				default: continue;
			};
			if (IsOperand(node->prev)) // a++
			{
				node->left = node->prev;
				NodesMemRemove(list,node->left);
			}
			else if (IsOperand(node->next)) // ++a
			{
				node->right = node->next;
				NodesMemRemove(list,node->right);
			}
			else goto MALFORMED;
		};
	};

	/// typical unary: *, -, ~, #, !, $
	for (ExprNode* node = list->last; node; node = node->prev) // Go backwards
	{
		char is_typecast = node->token->type == TypeCast;
		if (is_typecast || IsOperator(node))
		{
			if (!is_typecast) switch (node->token->opcode)
			{
				case Op_Mul: case Op_Subtract: if (IsOperand(node->prev)) continue; // a * b, a - b: don't treat as unary.
				case Op_BitNot: case Op_BoolNot: case Op_Stringify: case Op_Length: break;
				default: continue;
			};
			if (!IsOperand(node->next)) goto MALFORMED; // !a
			node->right = node->next;
			NodesMemRemove(list,node->right);
		};
	};

	/// << >>
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BitShiftLeft: case Op_BitShiftRight: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a << b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// &
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BitAnd: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a & b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// ^
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BitXor: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a ^ b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// |
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BitOr: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a | b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// * / %
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_Mul: case Op_Div: case Op_Modulus: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a * b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// + -
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_Add: case Op_Subtract: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a + b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// < <= > >=
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_LessThan: case Op_LessThanEqual: case Op_GreaterThan: case Op_GreaterThanEqual: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a < b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// == !=
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_Equal: case Op_Unequal: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a == b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// &&
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BoolAnd: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a && b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// ^^
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BoolXor: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a ^^ b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// ||
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_BoolOr: break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a || b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};

	/// = += -= *= /= %= &&= ||= ^^= &= |= ^= <<= >>=
	for (ExprNode* node = list->first; node; node = node->next)
	{
		if (IsOperator(node))
		{
			switch (node->token->opcode)
			{
				case Op_Assign:
				case Op_AddBy: case Op_SubtractBy: case Op_MulBy: case Op_DivBy: case Op_ModulusBy:
				case Op_BoolAndBy: case Op_BoolOrBy: case Op_BoolXorBy:
				case Op_BitAndBy: case Op_BitOrBy: case Op_BitXorBy: case Op_BitShiftLeftBy: case Op_BitShiftRightBy:
				break;
				default: continue;
			};
			if (!IsOperand(node->prev) || !IsOperand(node->next)) goto MALFORMED; // a = b
			node->left = node->prev;
			node->right = node->next;
			NodesMemRemove(list,node->left);
			NodesMemRemove(list,node->right);
		};
	};
};

ExprTree* AssembleExpression (swCompileErrors* errors, BlockData* scope, NodesMem* nodes_mem)
{
	for (ExprNode* node = nodes_mem->first; node; node = node->next)
	{
		if (node->token->type == ParenthesizedSection)
		{
			if (node->token->nodes_mem->len == 1)
			{
				swToken* token = node->token->nodes_mem->nodes->token;
				if (token->type == Identifier)
				{
					Type* type = ResolveTypeRecursive(scope,token);
					if (type)
					{
						NodesMemAbort(node->token->nodes_mem);
						node->token->type = TypeCast;
						node->token->typecast_type = type;

						// if the very next token is a FunctionCall, it wasn't supposed to be.
						ExprNode* next = node->next;
						if (next)
						{
							if (next->token->type == FunctionCall)
							{
								FunctionArgsList* list = next->token->function_call_args;
								if (list->count == 1)
								{
									next->token->type = ParenthesizedSection;
									next->token->nodes_mem = *list->nodes_mems;
									free(list->nodes_mems);
									free(list);
								};
							};
						};
						continue;
					};
				};
			};
			ExprTree* nested = AssembleExpression(errors,scope,node->token->nodes_mem);
			node->token->type = Expression;
			node->token->expr_tree = nested;
		}
		else if (node->token->type == FunctionCall)
		{
			FunctionArgsList* args = node->token->function_call_args;
			if (args)
			{
				for (int i = 0; i < args->count; i++)
					args->expressions[i] = AssembleExpression(errors,scope,args->nodes_mems[i]);
			};
		};
	};


	if (nodes_mem->len)
	{
		if (nodes_mem->len == 1) nodes_mem->first = nodes_mem->nodes; // if there's only one item don't iterate through precedence groups
		else
		{
			// Attempt to form this list into a tree!
			NodesMemAssemble(nodes_mem);
		};

		if (nodes_mem->len == 1)
		{
			//printf("Assembled expression:\n");
			//PrintNodesMem(nodes_mem,0);

			ExprTree* tree = malloc(sizeof(ExprTree));
			tree->root = nodes_mem->first;
			tree->nodes_mem = nodes_mem->nodes;

			free(nodes_mem);
			return tree;
		};
	};

	NodesMemAbort(nodes_mem);

	char* err_msg;
	asprintf(&err_msg,"Malformed expression.");
	swThrowCompileError(errors,err_msg,nodes_mem->start_line,nodes_mem->start_col);

	return NULL;
};

NodesMem* swReadExpr (swTokens* tokens, swCompileErrors* errors, enum TokenType end_expected, enum TokenType* alternate_end)
{
	// if alternate_end is non NULL, check for it, and if encountered, clear its value to indicate it was encountered.

	NodesMem* nodes_mem = malloc(sizeof(NodesMem));
	NodesMemInit(nodes_mem);

	swToken* first_token = swNextToken(tokens); // for error line number even if the expression is empty
	nodes_mem->start_line = first_token->line_number;
	nodes_mem->start_col = first_token->line_position;
	swUngetToken(tokens);

	enum TokenType prev_token_type = Token_None;

	while (1)
	{
		swToken* token = swNextToken(tokens);
		if (token->type == end_expected) break;
		if (alternate_end)
		{
			if (token->type == *alternate_end)
			{
				*alternate_end = 0;
				break;
			};
		};

		if (token->type == Operator || token->type == Identifier || token->type == IntLiteral || token->type == FloatLiteral || token->type == StringLiteral)
		{
			NodesMemAppend(nodes_mem,token);
		}
		else if (token->type == OpenParentheses)
		{
			if (prev_token_type != Operator && prev_token_type != Token_None) // (1) and a*(1,2) versus a(1,2) and (a)(1,2)
			{
				// It's a function call.
				token->type = FunctionCall;
				NodesMemAppend(nodes_mem,token);
				// token stream for x = f(5) would be x, =, f, functioncall
				// the args list would be a list of expressions that the FunctionCall token would have a pointer to

				// Now get the stuff inside the parentheses as args.
				token->function_call_args = ReadFunctionArgs(tokens,errors);
			}
			else
			{
				// It's either a subexpression, or typecast.
				// Determination of which it is will happen later in compilation. We're just gathering tokens right now.
				token->type = ParenthesizedSection;
				token->nodes_mem = swReadExpr(tokens,errors,CloseParentheses,NULL);
				NodesMemAppend(nodes_mem,token);
			};
		}
		else
		{
			char* err_msg;
			asprintf(&err_msg,"Unexpected token in expression."); /// To do: better error message
			swThrowCompileError(errors,err_msg,token->line_number,token->line_position);

			if (token->type == EndOfFile) break;
		};
		prev_token_type = token->type;
	};

	NodesMemSetup(nodes_mem);

	return nodes_mem;
};

void BuildActionExpressions (swCompileErrors* errors, BlockData* block, Action* action)
{
	if (action->type == Action_Statement)
	{
		action->statement_expr = AssembleExpression(errors,block,action->statement_nodesmem);
	}
	else if (action->type == Action_CodeBlock)
	{
		BuildBlockExpressions(errors,action->block);
		action->block->parent_block = block;
	}
	else if (action->type == Action_If)
	{
		action->if_header.cond_expr = AssembleExpression(errors,block,action->if_header.cond_nodesmem);
		if (action->if_header.within) BuildActionExpressions(errors,block,action->if_header.within);
		if (action->if_header.else_action) BuildActionExpressions(errors,block,action->if_header.else_action);
	}
	else if (action->type == Action_While || action->type == Action_OnceThenWhile)
	{
		action->while_header.cond_expr = AssembleExpression(errors,block,action->while_header.cond_nodesmem);
		if (action->while_header.within) BuildActionExpressions(errors,block,action->while_header.within);
	}
	else if (action->type == Action_Loop)
	{
		if (action->simple_loop_within) BuildActionExpressions(errors,block,action->simple_loop_within);
	}
	else if (action->type == Action_FuncDef)
	{
		BuildBlockExpressions(errors,action->funcdef_header.function_code);
		action->funcdef_header.function_code->parent_block = block;
	}
	else if (action->type == Action_Return)
	{
		if (action->return_nodesmem) action->return_expr = AssembleExpression(errors,block,action->return_nodesmem);
	};
};

void ExprTreeFree (ExprTree* tree)
{
	AbortExprNode(tree->root);
	free(tree->nodes_mem);
};

void BuildBlockExpressions (swCompileErrors* errors, BlockData* block)
{
	block->parent_block = NULL;
	for (size_t i = 0; i < block->action_count; i++)
	{
		Action* action = block->actions[i];
		BuildActionExpressions(errors,block,action);
	};
	for (size_t i = 0; i < block->type_count; i++)
	{
		Type* type = block->types + i;
		BuildBlockExpressions(errors,type->class_block);
		type->class_block->parent_block = block;
	};
};
