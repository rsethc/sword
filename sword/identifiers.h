#ifndef KEYWORDS_H_INCLUDED
#define KEYWORDS_H_INCLUDED

#include "lex.h"

char swEqualToNullTerminated (char* bufstr, int bufstrlen, char* nulstr);

int swIsIdentifierStart (char ch);
size_t swReadIdentifier (char* src, size_t* pos, size_t* line_number, size_t* line_position);
void swIdentifyKeyword (swToken* token);

#endif // KEYWORDS_H_INCLUDED
