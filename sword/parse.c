#define _GNU_SOURCE
#include <stdio.h>
#include "parse.h"
#include "compile-errors.h"
#include "operators.h"
#include "identifiers.h"
#include "lex.h"
#include "simplestorage.h"

swToken ident_ubyte = {Identifier,5,"ubyte"};
swToken ident_byte = {Identifier,4,"byte"};
swToken ident_ushort = {Identifier,6,"ushort"};
swToken ident_short = {Identifier,5,"short"};
swToken ident_uint = {Identifier,4,"uint"};
swToken ident_int = {Identifier,3,"int"};
swToken ident_ulong = {Identifier,5,"ulong"};
swToken ident_long = {Identifier,4,"long"};
swToken ident_float = {Identifier,5,"float"};
swToken ident_double = {Identifier,6,"double"};
swToken ident_string = {Identifier,6,"string"};
Type PrimitiveTypes [] =
{
	{1,Type_UnsignedByte,&ident_ubyte},
	{1,Type_SignedByte,&ident_byte},
	{1,Type_UnsignedShort,&ident_ushort},
	{1,Type_SignedShort,&ident_short},
	{1,Type_UnsignedInt,&ident_uint},
	{1,Type_SignedInt,&ident_int},
	{1,Type_UnsignedLong,&ident_ulong},
	{1,Type_SignedLong,&ident_long},
	{1,Type_String,&ident_float},
	{1,Type_Double,&ident_double},
	{1,Type_String,&ident_string},
};

Var* VarDeclare (BlockData* scope)
{
	if (scope->var_count >= scope->var_capacity)
		scope->vars = realloc(scope->vars,sizeof(Var) * (scope->var_capacity <<= 1));
	return scope->vars + scope->var_count++;
};

Type* TypeDefine (BlockData* scope)
{
	if (scope->type_count >= scope->type_capacity)
		scope->types = realloc(scope->types,sizeof(Type) * (scope->type_capacity <<= 1));
	return scope->types + scope->type_count++;
};

char SameIdentifier (swToken* ident0, swToken* ident1)
{
	if (ident0->len != ident1->len) return 0;
	for (int i = 0; i < ident0->len; i++)
		if (ident0->str[i] != ident1->str[i]) return 0;
	return 1;
};

Var* ScopeFindLocal (BlockData* scope, swToken* identifier)
{
	for (int i = 0; i < scope->var_count; i++)
	{
		Var* var = scope->vars + i;
		if (SameIdentifier(identifier,var->identifier)) return var;
	};
	return NULL;
};

Var* ScopeFindRecursive (BlockData* scope, swToken* identifier)
{
	while (scope)
	{
		Var* var = ScopeFindLocal(scope,identifier);
		if (var) return var;
		scope = scope->parent_block;
	};
	return NULL;
};

Type* ResolveTypePrimitive (swToken* identifier)
{
	Type* prim = PrimitiveTypes;
	int n_prim = sizeof(PrimitiveTypes)/sizeof(Type);
	for (int i = 0; i < n_prim; i++, prim++)
		if (SameIdentifier(identifier,prim->identifier))
			return prim;
	return NULL;
};

Type* ResolveTypeLocal (BlockData* scope, swToken* identifier)
{
	Type* prim = ResolveTypePrimitive(identifier);
	if (prim) return prim;

	for (int i = 0; i < scope->type_count; i++)
	{
		Type* type = scope->types + i;
		if (SameIdentifier(identifier,type->identifier)) return type;
	};
	return NULL;
};

Type* ResolveTypeRecursive (BlockData* scope, swToken* identifier)
{
	while (scope)
	{
		Type* type = ResolveTypeLocal(scope,identifier);
		if (type) return type;
		scope = scope->parent_block;
	};
	return NULL;
};

FunctionArgsList* ReadFunctionArgs (swTokens* tokens, swCompileErrors* errors)
{
	swToken* maybe_closeparen = swNextToken(tokens);
	if (maybe_closeparen->type == CloseParentheses) return NULL;
	swUngetToken(tokens); // put it back, it's part of the first argument

	FunctionArgsList* list = malloc(sizeof(FunctionArgsList));
	size_t capacity = 1;
	list->expressions = malloc(sizeof(void*) * capacity);
	list->count = 0;
	while (1)
	{
		if (list->count >= capacity) list->expressions = realloc(list->expressions,sizeof(void*) * (capacity <<= 1));
		enum TokenType close_paren = CloseParentheses;
		list->nodes_mems[list->count++] = swReadExpr(tokens,errors,Comma,&close_paren);
		if (!close_paren) break; // close parentheses encountered, args list is finished
		if (swNextToken(tokens)->type == EndOfFile) break;
		else swUngetToken(tokens);
	};
	return list;
};
void FuncArgsFree (FunctionArgsList* list)
{
	// by the time this would get called, the NodesMems already have become ExprTrees
	for (int i = 0; i < list->count; i++)
	{
		ExprTree* tree = list->expressions[i]; // it could be NULL
		if (tree) ExprTreeFree(tree);
	};
	free(list->expressions);
	free(list);
};
void AbortExprNode (ExprNode* node)
{
	if (node->left) AbortExprNode(node->left);
	if (node->right) AbortExprNode(node->right);

	if (node->token->type == ParenthesizedSection)
	{
		// won't be null:
		NodesMemAbort(node->token->nodes_mem);
	}
	else if (node->token->type == FunctionCall)
	{
		// could be null:
		if (node->token->function_call_args) FuncArgsFree(node->token->function_call_args);
	};
};


void AppendAction (BlockData* block, Action* action)
{
	if (block->action_count >= block->action_capacity)
		block->actions = realloc(block->actions,sizeof(Action*) * (block->action_capacity <<= 1));
	block->actions[block->action_count++] = action;
};

void Declaration (swTokens* tokens, swCompileErrors* compile_errors, BlockData* scope, swToken* type_name)
{
	while (1)
	{
		swToken* varname;
		int pointer_level = 0;
		while (1)
		{
			varname = swNextToken(tokens);
			if (varname->type == Operator && varname->opcode == Op_Mul) pointer_level++;
			else break;
		};

		if (varname->type != Identifier)
		{
			// Impossible (at least for the moment) to happen because of the code that calls this function.
			char* err_msg;
			asprintf(&err_msg,"Expected identifier, got some other crap instead."); /// to do: more helpful error message
			swThrowCompileError(compile_errors,err_msg,varname->line_number,varname->line_position);
			break;
		};

		Var* var = VarDeclare(scope);
		var->identifier = varname;
		var->type_name = type_name;
		var->pointer_level = pointer_level;

		swToken* after = swNextToken(tokens);
		if (after->type == Semicolon) return;
		if (after->type == Comma) continue; /// Another variable of the same type.
		if (after->type == Operator && after->opcode == Op_Assign)
		{
			// if we've grabbed "x" "=", put both of those back so that they are part of the expression.
			swUngetToken(tokens);
			swUngetToken(tokens);
			Action* assignment = malloc(sizeof(Action));
			AppendAction(scope,assignment);
			assignment->type = Action_Statement;
			enum TokenType comma_ending = Comma;
			assignment->statement_nodesmem = swReadExpr(tokens,compile_errors,Semicolon,&comma_ending);
			if (!comma_ending) continue; // Comma encountered
			else break; // Semicolon encountered
		};

		// Invalid token after the varname
		char* err_msg;
		asprintf(&err_msg,"Expected ; or =, got some other crap instead."); /// to do: more helpful error message
		swThrowCompileError(compile_errors,err_msg,after->line_number,after->line_position);
		break;
	};
};
BlockData* NewBlockData ()
{
	BlockData* out = malloc(sizeof(BlockData));
	// out->parent_block = parent_block; // This gets set up in a later parse stage.
	out->action_count = 0;
	out->action_capacity = 1;
	out->actions = malloc(sizeof(Action*) * out->action_capacity);
	out->type_count = 0;
	out->type_capacity = 1;
	out->types = malloc(sizeof(Type) * out->type_capacity);
	out->var_count = 0;
	out->var_capacity = 1;
	out->vars = malloc(sizeof(Var) * out->var_capacity);
	return out;
};
char Expect (swTokens* tokens, swCompileErrors* compile_errors, enum TokenType expected)
{
	swToken* here = swNextToken(tokens);
	if (here->type == expected) return 0;

	char* err_msg;
	asprintf(&err_msg,"expected (, instead got something else"); /// to do: improve error message
	swThrowCompileError(compile_errors,err_msg,here->line_number,here->line_position);
	swUngetToken(tokens);
	return 1;
};
Action* ReadAction (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block, char can_declare, char can_define, char can_control);
Action* ReadIf (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block)
{
	Expect(tokens,compile_errors,OpenParentheses);

	Action* if_action = malloc(sizeof(Action));
	if_action->type = Action_If;
	if_action->if_header.cond_nodesmem = swReadExpr(tokens,compile_errors,CloseParentheses,NULL);

	if_action->if_header.within = ReadAction(tokens,compile_errors,block,0,0,1);

	swToken* maybe_else = swNextToken(tokens);
	if (maybe_else->type == Keyword_Else)
	{
		if_action->if_header.else_action = ReadAction(tokens,compile_errors,block,0,0,1);
	}
	else
	{
		swUngetToken(tokens); // put back the thing that wasn't an "else".
		if_action->if_header.else_action = NULL;
	};

	return if_action;
};
Action* ReadLoop (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block)
{
	Action* loop_action = malloc(sizeof(Action));
	loop_action->type = Action_Loop;

	loop_action->simple_loop_within = ReadAction(tokens,compile_errors,block,0,0,1);

	return loop_action;
};
Action* ReadWhile (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block)
{
	Expect(tokens,compile_errors,OpenParentheses);

	Action* while_action = malloc(sizeof(Action));
	while_action->type = Action_While;
	while_action->while_header.cond_nodesmem = swReadExpr(tokens,compile_errors,CloseParentheses,NULL);

	while_action->while_header.within = ReadAction(tokens,compile_errors,block,0,0,1);

	return while_action;
};
Action* ReadOnceThenWhile (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block)
{
	Expect(tokens,compile_errors,Keyword_Then);
	Expect(tokens,compile_errors,Keyword_While);

	Expect(tokens,compile_errors,OpenParentheses);

	Action* while_action = malloc(sizeof(Action));
	while_action->type = Action_OnceThenWhile;
	while_action->while_header.cond_nodesmem = swReadExpr(tokens,compile_errors,CloseParentheses,NULL);

	while_action->while_header.within = ReadAction(tokens,compile_errors,block,0,0,1);

	return while_action;
};

Action* ReadFor (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block)
{
	// Create a mezzanine block. Then create a while block inside it.
	/// for (init; cond; after, after, after) StatementOrBlockOrNothing
	/// { init; while (cond) { code; after; after; after; } }

	Expect(tokens,compile_errors,OpenParentheses);

	Action* outer_block_action = malloc(sizeof(Action));
	outer_block_action->type = Action_CodeBlock;
	outer_block_action->block = NewBlockData();
	BlockData* outer_block = outer_block_action->block;

	swToken* maybe_typename = swNextToken(tokens);
	swToken* maybe_varname = swNextToken(tokens);
	swUngetToken(tokens); // even if it's a decl, the varname must be picked up by Declaration
	if (maybe_typename->type == Identifier && maybe_varname->type == Identifier)
	{
		Declaration(tokens,compile_errors,outer_block,maybe_typename);
	}
	else if (maybe_typename->type != Semicolon) // if it is empty statement for(;;), it's simply nothing added to the outer block
	{
		swUngetToken(tokens); // it's not a type name, it's part of the expression, put it back too
		Action* init_statement = malloc(sizeof(Action));
		AppendAction(outer_block,init_statement);
		init_statement->type = Action_Statement;
		init_statement->statement_nodesmem = swReadExpr(tokens,compile_errors,Semicolon,NULL);
	};

	Action* inner_loop = malloc(sizeof(Action));
	AppendAction(outer_block,inner_loop);
	Action** within_ptr;
	swToken* maybe_condempty = swNextToken(tokens);
	if (maybe_condempty->type == Semicolon)
	{
		// No condition, it's a simple "loop" construct
		inner_loop->type = Action_Loop;
		within_ptr = &inner_loop->simple_loop_within;
	}
	else
	{
		swUngetToken(tokens); // there is a condition, it's a "while" construct
		inner_loop->type = Action_While;
		inner_loop->while_header.cond_nodesmem = swReadExpr(tokens,compile_errors,Semicolon,NULL);
		within_ptr = &inner_loop->while_header.within;
	};

	Action* loop_block_action = malloc(sizeof(Action));
	loop_block_action->type = Action_CodeBlock;
	loop_block_action->block = NewBlockData();
	*within_ptr = loop_block_action;

	Action* inner_block_action = malloc(sizeof(Action));
	AppendAction(loop_block_action->block,inner_block_action);
	inner_block_action->type = Action_CodeBlock;
	inner_block_action->block = NewBlockData();

	Action* afters_block_action = malloc(sizeof(Action));
	AppendAction(loop_block_action->block,afters_block_action);
	afters_block_action->type = Action_CodeBlock;
	afters_block_action->block = NewBlockData();

	swToken* next = swNextToken(tokens);
	if (next->type != CloseParentheses)
	{
		swUngetToken(tokens);
		while (1)
		{
			Action* after_statement = malloc(sizeof(Action));
			AppendAction(afters_block_action->block,after_statement);
			after_statement->type = Action_Statement;
			enum TokenType comma = Comma;
			after_statement->statement_nodesmem = swReadExpr(tokens,compile_errors,CloseParentheses,&comma);
			if (comma) break; // meaning we did not hit a comma
		};
	};

	Action* within = ReadAction(tokens,compile_errors,inner_block_action->block,0,0,1);
	if (within) AppendAction(inner_block_action->block,within);

	return outer_block_action;
};
swToken StaticAssignToken;
Action* ReadAction (swTokens* tokens, swCompileErrors* compile_errors, BlockData* block, char can_declare, char can_define, char can_control)
{
	// can_declare: is variable declaration allowed (including function *declaration*).
		// Prevents: while (int x = 5) x++;
	// can_define: is class definition or function *definition* allowed.
		// Prevents: while (true) class X { int a; };
	// can_control: can have control flow statement such as if statement, as well as nested blocks.
		// Prevents: int y = x; for (if (x == 5) y++; y < 10; y++) x--;
		// Prevents: if ({x == 2;}) x++;

	swToken* token = swNextToken(tokens);

	if (token->type == Keyword_If)
	{
		if (!can_control)
		{
			char* err_msg;
			asprintf(&err_msg,"'if' where it is not allowed"); /// improve this
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		return ReadIf(tokens,compile_errors,block);
	}
	else if (token->type == Keyword_While)
	{
		if (!can_control)
		{
			char* err_msg;
			asprintf(&err_msg,"'while' where it is not allowed"); /// improve this
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		return ReadWhile(tokens,compile_errors,block);
	}
	else if (token->type == Keyword_Once)
	{
		if (!can_control)
		{
			char* err_msg;
			asprintf(&err_msg,"'once' where it is not allowed"); /// improve this
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		return ReadOnceThenWhile(tokens,compile_errors,block);
	}
	else if (token->type == Keyword_For)
	{
		if (!can_control)
		{
			char* err_msg;
			asprintf(&err_msg,"'for' where it is not allowed"); /// improve this
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		return ReadFor(tokens,compile_errors,block);
	}
	else if (token->type == Keyword_Loop)
	{
		if (!can_control)
		{
			char* err_msg;
			asprintf(&err_msg,"'loop' where it is not allowed"); /// improve this
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		return ReadLoop(tokens,compile_errors,block);
	}
	else if (token->type == OpenBraces)
	{
		if (!can_control)
		{
			char* err_msg;
			asprintf(&err_msg,"code block where it is not allowed"); /// improve this
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		// It's a block, but not associated with if/while etc.
		Action* action = malloc(sizeof(Action));
		action->type = Action_CodeBlock;
		action->block = ReadBlock(tokens,compile_errors,CloseBraces);
		return action;
	}
	else if (token->type == Keyword_Goto)
	{
		Action* action = malloc(sizeof(Action));
		action->type = Action_Goto;
		action->label_name = swNextToken(tokens);
		Expect(tokens,compile_errors,Semicolon);
		return action;
	}
	else if (token->type == Keyword_Break)
	{
		Action* action = malloc(sizeof(Action));
		action->type = Action_Break;
		Expect(tokens,compile_errors,Semicolon);
		return action;
	}
	else if (token->type == Keyword_Continue)
	{
		Action* action = malloc(sizeof(Action));
		action->type = Action_Continue;
		Expect(tokens,compile_errors,Semicolon);
		return action;
	}
	else if (token->type == Keyword_Class)
	{
		if (!can_define)
		{
			char* err_msg;
			asprintf(&err_msg,"class definition not allowed here");
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		swToken* class_name = swNextToken(tokens);
		if (class_name->type != Identifier)
		{
			swUngetToken(tokens);
			char* err_msg;
			asprintf(&err_msg,"class name expected, got some other crap instead");
			swThrowCompileError(compile_errors,err_msg,class_name->line_number,class_name->line_position);
		};

		Expect(tokens,compile_errors,OpenBraces);

		// class is not an action, it's a type. but it's still a code block.
		Type* class_type = TypeDefine(block);
		class_type->variant = Class;
		class_type->identifier = class_name;
		class_type->class_block = ReadBlock(tokens,compile_errors,CloseBraces);

		return NULL;
	}
	else if (token->type == Keyword_Function)
	{
		if (!can_declare)
		{
			char* err_msg;
			asprintf(&err_msg,"function declaration not allowed here");
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
		};

		int pointer_level = 0;
		while (1)
		{
			swToken* maybe_ptr = swNextToken(tokens);
			if (maybe_ptr->type == Operator && maybe_ptr->opcode == Op_Mul) pointer_level++;
			else
			{
				swUngetToken(tokens);
				break;
			};
		};

		swToken* func_name = swNextToken(tokens);
		if (func_name->type != Identifier)
		{
			swUngetToken(tokens);
			char* err_msg;
			asprintf(&err_msg,"function name expected, got some other crap instead");
			swThrowCompileError(compile_errors,err_msg,func_name->line_number,func_name->line_position);
		};

		FunctionSignature* signature = malloc(sizeof(FunctionSignature));
		signature->arg_count = 0;
		signature->arg_vars = NULL;

		swToken* next_token = swNextToken(tokens);
		if (next_token->type == OpenParentheses) // if not, there is no args list (legal).
		{
			// args list
			next_token = swNextToken(tokens);
			if (next_token->type == CloseParentheses); // explicit empty args list
			else
			{
				swUngetToken(tokens);
				int arg_capacity = 1;
				signature->arg_vars = malloc(sizeof(Var) * arg_capacity);
				while (1)
				{
					if (signature->arg_count >= arg_capacity)
						signature->arg_vars = realloc(signature->arg_vars,sizeof(Var) * (arg_capacity <<= 1));

					Var* var = signature->arg_vars + signature->arg_count++;
					var->type_name = swNextToken(tokens);
					var->identifier = swNextToken(tokens);

					next_token = swNextToken(tokens);
					if (next_token->type == Comma) continue;
					else if (next_token->type == CloseParentheses) break;
					else if (next_token->type == EndOfFile)
					{
						char* err_msg;
						asprintf(&err_msg,"expected close parentheses, got EOF");
						swThrowCompileError(compile_errors,err_msg,next_token->line_number,next_token->line_position);
						break;
					};
				};
			};
			next_token = swNextToken(tokens);
		};
		if (next_token->type == Operator && next_token->opcode == Op_RightArrow)
		{
			// return type
			signature->return_typename = swNextToken(tokens);
			next_token = swNextToken(tokens);
		}
		else signature->return_typename = NULL;
		Var* func_var = VarDeclare(block);
		func_var->identifier = func_name;
		func_var->type_name = NULL;
		func_var->func_signature = signature;
		func_var->pointer_level = pointer_level;
		if (next_token->type == Semicolon)
		{
			// function pointer *only*, no definition
			return NULL;
		}
		else
		{
			swUngetToken(tokens);
			Expect(tokens,compile_errors,OpenBraces);

			// function definition, set the function pointer variable to it.
			Action* action = malloc(sizeof(Action));
			action->type = Action_FuncDef;
			action->funcdef_header.function_name = func_name;
			action->funcdef_header.function_code = ReadBlock(tokens,compile_errors,CloseBraces);
			return action;
		};


		return NULL;
	}
	else if (token->type == Keyword_Return)
	{
		Action* action = malloc(sizeof(Action));
		action->type = Action_Return;
		swToken* maybe_semicolon = swNextToken(tokens);
		if (maybe_semicolon->type == Semicolon)
		{
			action->return_nodesmem = NULL;
		}
		else
		{
			swUngetToken(tokens);
			action->return_nodesmem = swReadExpr(tokens,compile_errors,Semicolon,NULL);
		};
		return action;
	}
	else if (token->type == Semicolon) return NULL; // don't emit anything. ;;;;;;; is valid code.
	else
	{
		if (token->type == Identifier)
		{
			swToken* maybe_varname = swNextToken(tokens);
			swUngetToken(tokens);
			if (maybe_varname->type == Identifier)
			{
				// It's a type declaration.
				if (!can_declare)
				{
					char* err_msg;
					asprintf(&err_msg,"Variable declaration not allowed here");
					swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
				};
				Declaration(tokens,compile_errors,block,token);
				return NULL;
			}
			else if (maybe_varname->type == Operator && maybe_varname->opcode == Op_ChooseSecond /* : */)
			{
				// it's a jump label, which in a way is like a declaration.
				if (!can_declare)
				{
					char* err_msg;
					asprintf(&err_msg,"Jump label not allowed here");
					swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);
				};
				Action* label = malloc(sizeof(Action));
				label->type = Action_Label;
				label->label_name = token;
				return label;
			};
		};
		// Try to deal with it as an expression.
		swUngetToken(tokens); // `token` was part of the expression, put it back so ReadExpr can see it.
		Action* action = malloc(sizeof(Action));
		action->type = Action_Statement;
		action->statement_nodesmem = swReadExpr(tokens,compile_errors,Semicolon,NULL);
		return action;
	};
};
BlockData* ReadBlock (swTokens* tokens, swCompileErrors* compile_errors, enum TokenType end_token_type)
{
	BlockData* block = NewBlockData();
	while (1)
	{
		swToken* token = swNextToken(tokens);
		if (token->type == end_token_type) break;
		else if (token->type == EndOfFile)
		{
			// If end_token_type is EndOfFile then the if,elseif order above means we don't hit this section.
			// So, if we do hit this section, it means we were expecting a CloseBraces and will never get it.
			// Which of course means there was unfinished business and the code is not legal!
			char* err_msg;
			asprintf(&err_msg,"EOF encountered before it was expected");
			swThrowCompileError(compile_errors,err_msg,token->line_number,token->line_position);

			break;
		}
		else
		{
			// There's something here. Put it back and interpret the thing that it begins.
			swUngetToken(tokens);
			Action* action = ReadAction(tokens,compile_errors,block,1,1,1);
			if (action) AppendAction(block,action);
		};
	};
	return block;
};

