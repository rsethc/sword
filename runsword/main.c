#define _GNU_SOURCE
#include <stdio.h>
#include "sword/lex.h"
#include "sword/comments.h"
#include "sword/compile-errors.h"
#include "sword/compile.h"

void swCompile (char* src)
{
	swCompileErrors compile_errors;
	swInitCompileErrors(&compile_errors);

	swRemoveComments(src,&compile_errors,NULL);

	swTokens tokens;
	swInitTokens(&tokens);

	swReadAllTokens(&tokens,src,&compile_errors,0);

	swPrintTokens(&tokens);

	swCompileTokens(&tokens,&compile_errors);

	swQuitTokens(&tokens);

	printf("%d compile errors%c\n",(int)compile_errors.count,compile_errors.count ? ':' : '.');
	for (int i = 0; i < compile_errors.count; i++)
	{
		swCompileError* error = compile_errors.errors + i;
		printf("\t%d:%d - %s\n",(int)error->line,(int)error->col,error->message);
	};
	swQuitCompileErrors(&compile_errors);
};

int main ()
{
	FILE* file = fopen("test.sword","rb");
	if (!file) return -2;

	fseek(file,0,SEEK_END);
	size_t len = ftell(file);
	fseek(file,0,SEEK_SET);

	char* source = malloc(len + 1);

	fread(source,1,len,file);
	fclose(file);
	source[len] = 0;

	swCompile(source);

	free(source);
	return 0;
};
