CFLAGS = -O2 -s -fomit-frame-pointer -Werror=implicit-function-declaration 

SWORD_OBJS = $(patsubst %.c,%.o,$(shell find sword -iname "*.c"))

all: libsword.a 
	$(CC) $(CFLAGS) -o runsword/runsword -I . -L . runsword/main.c -lsword 

libsword.a: $(SWORD_OBJS) 
	ar -cr $@ $^

%.o: %.c
	$(CC) -c $(CFLAGS) -fPIC $< -o $@ 
	
clean: 
	@rm -rvf $(shell find . -iname '*.o') *.a 